<?php

function commerce_shipping_eu_get_region_list() {
  $regions = array();
  $regions['uk'] = 'UK';
  $regions['channel_islands'] = 'Channel Islands';
  $regions['europe_eu'] = 'Europe (EU)';
  $regions['europe_other'] = 'Europe (Non-EU)';
  $regions['north_america'] = 'North America';
  $regions['row'] = 'Rest of World';
  return $regions;
}

/**
 * Returns a list of countries (two-letter codes and names) per region,
 * used by the Rules condition for determining the correct shipping service.
 *
 * Note that only the two-letter codes are used, the names are there for
 * informative purposes only.
 */
function commerce_shipping_eu_get_countries_by_region() {
  return array(
    'uk' => array(
      'GB' => 'United Kingdom',
    ),
    'channel_islands' => array(
      'GG' => 'Guernsey',
      'JE' => 'Jersey',
      'IM' => 'Isle of Man',
    ),
    'europe_eu' => array(
      'AT' => 'Austria',
      'BE' => 'Belgium',
      'BG' => 'Bulgaria (Republic)',
      'CY' => 'Cyprus',
      'CZ' => 'Czech Republic',
      'DK' => 'Denmark',
      'EE' => 'Estonia',
      'FI' => 'Finland',
      'FR' => 'France',
      'DE' => 'Germany',
      'GR' => 'Greece',
      'HU' => 'Hungary',
      'IE' => 'Ireland (Republic)',
      'IT' => 'Italy',
      'LV' => 'Latvia',
      'LT' => 'Lithuania',
      'LU' => 'Luxembourg',
      'MT' => 'Malta',
      'NL' => 'Netherlands',
      'PL' => 'Poland',
      'PT' => 'Portugal',
      'RO' => 'Romania',
      'SK' => 'Slovakia',
      'SI' => 'Slovenia',
      'ES' => 'Spain',
      'SE' => 'Sweden',
    ),
    'europe_other' => array(
      'AL' => 'Albania',
      'AD' => 'Andorra',
      'BY' => 'Belarus',
      'BA' => 'Bosnia',
      'HR' => 'Croatia',
      'FO' => 'Faroe Islands',
      'GI' => 'Gibraltar',
      'IS' => 'Iceland',
      'LI' => 'Liechtenstein',
      'MK' => 'Macedonia',
      'MD' => 'Moldova',
      'MC' => 'Monaco',
      'ME' => 'Montenegro',
      'NO' => 'Norway',
      'RU' => 'Russia',
      'SM' => 'San Marino',
      'RS' => 'Serbia',
      'SJ' => 'Svalbard and Jan Mayen Islands',
      'CH' => 'Switzerland',
      'TR' => 'Turkey',
      'UA' => 'Ukraine',
      'VA' => 'Vatican',
    ),
    'north_america' => array(
      'CA' => 'Canada',
      'US' => 'United States',
    ),
  );
}
