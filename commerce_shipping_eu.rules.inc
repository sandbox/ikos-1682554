<?php

/**
 * Implements hook_rules_condition_info().
 */
function commerce_shipping_eu_rules_condition_info() {
  $conditions = array();

  $conditions['commerce_shipping_eu_compare_address_region'] = array(
    'label' => t('Order address region comparison'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('The order containing the profile reference with the address in question.'),
      ),
      'address_field' => array(
        'type' => 'text',
        'label' => t('Address'),
        'options list' => 'commerce_shipping_eu_order_address_field_options_list',
        'description' => t('The address associated with this order whose component you want to compare.'),
        'restriction' => 'input',
      ),
      'value' => array(
        'type' => 'text',
        'label' => t('Value'),
        'description' => t('The region the address is in.'),
        'options list' => 'commerce_shipping_eu_get_region_list',
      ),
    ),
    'group' => t('Commerce Order'),
    'callbacks' => array(
      'execute' => 'commerce_shipping_eu_rules_order_region',
    ),
  );

  return $conditions;
}

/**
 * Condition callback: compares an address component against the given region.
 */
function commerce_shipping_eu_rules_order_region($order, $address_field, $value) {
  list($field_name, $address_field_name) = explode('|', $address_field);

  // If we actually received a valid order...
  if (!empty($order)) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);

    // And if we can actually find the requested address data...
    if (!empty($wrapper->{$field_name}) && !empty($wrapper->{$field_name}->{$address_field_name})) {
      $address = $wrapper->{$field_name}->{$address_field_name}->value();

      // get the country
      $country = $address['country'];

      // now we have to check if the country is in the region specified in $value
      if (commerce_shipping_eu_country_in_region($country, $value)) return TRUE;

    }
  }

  return FALSE;
}

/**
 * Rules condition: checks to see if the given order belongs to the specified region.
 */
function commerce_shipping_eu_country_in_region($country, $region) {
  $countries = commerce_shipping_eu_get_countries_by_region();

  // Special handling for "other"", which translates to "doesn't belong anywhere".
  if ($region == 'row') {
    $other = TRUE;
    foreach ($countries as $region => $countries_by_region) {
      if (!empty($countries[$region][$country])) {
        $other = FALSE;
      }
    }
    return $other;
  }
  else {
    return !empty($countries[$region][$country]);
  }
}


/**
 * Options list callback: address fields for the address comparison condition.
 */
function commerce_shipping_eu_order_address_field_options_list() {
  $options = array();

  // Retrieve a list of all address fields on customer profile bundles.
  $address_fields = commerce_info_fields('addressfield', 'commerce_customer_profile');

  // Loop over every customer profile reference field on orders.
  foreach (commerce_info_fields('commerce_customer_profile_reference', 'commerce_order') as $field_name => $field) {
    // Retrieve the type of customer profile referenced by this field.
    $type = $field['settings']['profile_type'];

    // Loop over every address field looking for any attached to this bundle.
    foreach ($address_fields as $address_field_name => $address_field) {
      if (in_array($type, $address_field['bundles']['commerce_customer_profile'])) {
        // Add it to the options list.
        $instance = field_info_instance('commerce_customer_profile', 'commerce_customer_address', $type);

        $options[commerce_customer_profile_type_get_name($type)][$field_name . '|' . $address_field_name] = check_plain($instance['label']);
      }
    }
  }

  if (empty($options)) {
    drupal_set_message(t('No order addresses could be found for comparison.'), 'error');
  }

  return $options;
}